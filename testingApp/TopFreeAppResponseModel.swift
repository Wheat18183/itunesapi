//
//  TopFreeAppResponseModel.swift
//  testingApp
//
//  Created by Tsz Shan Mak on 14/8/2020.
//

import UIKit
import ObjectMapper

struct TopFreeAppResponseModel: Mappable {
    var appList: Array<AppInfoItem>?
    var categoryName: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        categoryName <- map["feed.title.label"]
        appList <- map["feed.entry"]
    }

}

struct AppInfoItem: Mappable {
    var appName: String?
    var genresString: String?
    var appIconList: Array<AppIconItem>?
    var rating: Float?
    var ratingCount: Int?
    var appId: String?
    var author: String?
    var summary: String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        appName <- map["im:name.label"]
        genresString <- map["category.attributes.label"]
        appIconList <- map["im:image"]
        appId <- map["id.attributes.im:id"]
        summary <- map["summary.label"]
        author <- map["im:artist.label"]
    }
    
}


struct AppIconItem: Mappable {
    var iconUrl: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        iconUrl <- map["label"]
    }
    
}
