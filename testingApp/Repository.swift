//
//  Repository.swift
//  testingApp
//
//  Created by Tsz Shan Mak on 12/8/2020.
//

import UIKit
import RxSwift

protocol Repository {
    func getObjects() -> Observable<[TopGrossingResponseModel]>
}

