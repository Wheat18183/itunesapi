//
//  AppDetailResponsetModel.swift
//  Alamofire
//
//  Created by Tsz Shan Mak on 15/8/2020.
//


import UIKit
import ObjectMapper

struct AppDetailResponsetModel: Mappable {
    var result: Array<AppDetailItem>?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        result <- map["results"]
        
    }

}
struct AppDetailItem: Mappable {
    var appRating: Float?
    var ratingCount: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        appRating <- map["averageUserRating"]
        ratingCount <- map["userRatingCount"]
    }

}
