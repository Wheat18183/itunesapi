//
//  TopGrossingResponseModel.swift
//  testingApp
//
//  Created by Tsz Shan Mak on 12/8/2020.
//

import UIKit
import ObjectMapper

struct TopGrossingResponseModel: Mappable {
    var appList: Array<AppInfoItem>?
    var categoryName: String?
    

    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        appList <- map["feed.entry"]
        categoryName <- map["feed.title.label"]
        if appList == nil { appList = [] }
        if categoryName == nil { categoryName = "" }
    }
    
}

