//
//  ViewController.swift
//  testingApp
//
//  Created by Tsz Shan Mak on 12/8/2020.
//

import UIKit
import RxSwift
import Kingfisher

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UISearchBarDelegate {
    
    var vm = HomeViewModel()
    
    //@IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var recommandAppView: UIView!
    @IBOutlet weak var recommandAppTitleLabel: UILabel!
    @IBOutlet weak var recommandAppCollectionView: UICollectionView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var suggestionTableView: UITableView!
    
    
    var disposeBag = DisposeBag()
    var disposables: Disposable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initUI()
        bindUI()
        fetchData()
        
    }//☆
    
    func initUI() {
        overrideUserInterfaceStyle = .light
        
    }
    
    func bindUI() {
        self.vm.topGrossingAppLoading.subscribe(onNext: { [weak self] loading in
            self?.loadingView.isHidden = !loading
            self?.recommandAppView.isHidden = loading
            self?.recommandAppTitleLabel.text = "推介"
            //self?.recommandAppTitleLabel.text = self?.vm.topGrossingResponseModel?.categoryName ?? ""
            self?.recommandAppCollectionView.reloadData()
            self?.mainTableView.updateConstraints()
            
        }).disposed(by: self.disposeBag)
        
        self.vm.topfreeAppLoading.subscribe(onNext: { [weak self] loading in
            self?.loadingView.isHidden = !loading
            self?.mainTableView.isHidden = loading
            self?.mainTableView.reloadData()
            
        }).disposed(by: self.disposeBag)
    }
    
    func fetchData() {
        self.vm.requestData()
    }
    
    //MARK: - UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.mainTableView {
            return self.vm.topFreeAppList?.count ?? 0
        } else {
            return self.vm.searchResultList?.count ?? 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.mainTableView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopFreeAppTableviewCell", for: indexPath) as! TopFreeAppTableviewCell
        cell.disposable?.dispose()
        cell.disposable = nil
        if cell.ratingStv.arrangedSubviews.count > 0 {
            cell.ratingStv.arrangedSubviews.forEach { (view) in
                cell.ratingStv.removeArrangedSubview(view)
            }
        }
        if let appInfo:AppInfoItem = self.vm.topFreeAppList?[indexPath.row] {
            
            cell.indexLabel.text = String(indexPath.row + 1)
            cell.appNameLabel.text = appInfo.appName ?? ""
            cell.genresLabel.text = appInfo.genresString ?? ""
            cell.appIconImageView.image = UIImage(named: "placeholder")
            //let processor = DownsamplingImageProcessor(size: cell.appIconImageView.bounds.size)
            if let imageList = appInfo.appIconList, imageList[imageList.count - 1].iconUrl?.count ?? 0 > 0{
                cell.appIconImageView.kf.setImage(with:  URL(string: imageList[imageList.count - 1].iconUrl!), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.3))//.cacheOriginalImage
                ])
            }
            if indexPath.row % 2 == 0 {
                //even
                cell.appIconImageView.layer.cornerRadius = cell.appIconImageView.layer.bounds.height / 4
            } else {
                //odd
                cell.appIconImageView.layer.cornerRadius = cell.appIconImageView.layer.bounds.height / 2
            }
            
            if appInfo.appId != nil {
                if appInfo.appName == "Telegram Messenger" {
                    
                }
                if appInfo.rating == nil && cell.disposable == nil{
                    cell.requestAppInfo(appId: appInfo.appId!, index: indexPath.row)
                    cell.appDetailData.subscribe(onNext: { [weak self] result in
                        //                    print(result)
                        if let resultModel = result as? AppDetailResponsetModel, let resultItem = resultModel.result?[0] {
                            self?.vm.topFreeApppResponseModel?.appList?[indexPath.row].rating = resultItem.appRating ?? 0.0
                            self?.vm.topFreeAppList?[indexPath.row].rating = resultItem.appRating ?? 0.0
                            self?.vm.topFreeApppResponseModel?.appList?[indexPath.row].ratingCount = resultItem.ratingCount ?? 0
                            self?.vm.topFreeAppList?[indexPath.row].ratingCount = resultItem.ratingCount ?? 0
                            
                            for index in 0...4 {
                                if Int(resultItem.appRating ?? 0) > index {
                                    let starLabel = UILabel()
                                    starLabel.text = "★"
                                    starLabel.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                                    starLabel.font = starLabel.font.withSize(14.0)
                                    cell.ratingStv.addArrangedSubview(starLabel)
                                } else {
                                    let starLabel = UILabel()
                                    starLabel.text = "☆"
                                    starLabel.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                                    starLabel.font = starLabel.font.withSize(14.0)
                                    cell.ratingStv.addArrangedSubview(starLabel)
                                }
                            }
                            cell.ratingCountLabel.text = "(\(resultItem.ratingCount ?? 0))"
                            cell.disposable?.dispose()
                            cell.disposable = nil
                            //cell.reloadInputViews()
                        }
                    })
                } else {
                    
                    for index in 0...4 {
                        if Int(appInfo.rating ?? 0) > index {
                            let starLabel = UILabel()
                            starLabel.text = "★"
                            starLabel.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                            starLabel.font = starLabel.font.withSize(14.0)
                            cell.ratingStv.addArrangedSubview(starLabel)
                            
                        } else {
                            let starLabel = UILabel()
                            starLabel.text = "☆"
                            starLabel.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                            starLabel.font = starLabel.font.withSize(14.0)
                            cell.ratingStv.addArrangedSubview(starLabel)
                            
                        }
                    }
                    cell.ratingCountLabel.text = "(\(appInfo.ratingCount ?? 0))"
                    //cell.reloadInputViews()
                }
            }
        }
        return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
            if let appInfo = self.vm.searchResultList?[indexPath.row] {
                
                cell.appNameLabel.text = appInfo.appName ?? ""
                cell.genresLabel.text = appInfo.genresString ?? ""
                cell.appIconImageView.image = UIImage(named: "placeholder")
                cell.appIconImageView.layer.cornerRadius = cell.appIconImageView.layer.bounds.height/4
                if let imageList = appInfo.appIconList, imageList[imageList.count - 1].iconUrl?.count ?? 0 > 0{
                    cell.appIconImageView.kf.setImage(with:  URL(string: imageList[imageList.count - 1].iconUrl!), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.3))//.cacheOriginalImage
                    ])
                }
                cell.summaryLabel.text = appInfo.summary ?? ""
                cell.authorLabel.text = appInfo.author ?? ""
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == self.mainTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopFreeAppTableviewCell", for: indexPath) as! TopFreeAppTableviewCell
            cell.disposable?.dispose()
            cell.disposable = nil
            
            if cell.ratingStv.arrangedSubviews.count > 0 {
                cell.ratingStv.arrangedSubviews.forEach { (view) in
                    cell.ratingStv.removeArrangedSubview(view)
                }
            }
        }
    }
    //MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            self.searchBar.resignFirstResponder()
            self.searchBar.text = nil
            self.vm.searchResultList = nil
            if (scrollView.contentOffset.y + UIScreen.main.bounds.height) > scrollView.contentSize.height - 300 {
                guard (self.vm.topFreeApppResponseModel?.appList?.count ?? 0) > 0 && (self.vm.topFreeAppList?.count ?? 0) > 0 else {
                    return
                }
                guard self.vm.topFreeAppList?.count ?? 0 < self.vm.topFreeApppResponseModel?.appList?.count ?? 0 else {
                    return
                }
                self.loadingView.isHidden = false
                for index in (self.vm.topFreeAppList?.count ?? 0)...((self.vm.topFreeAppList?.count ?? 0) + 10) {
                    guard index < self.vm.topFreeApppResponseModel?.appList?.count ?? 0 else {
                        self.mainTableView.reloadData()
                        self.loadingView.isHidden = true
                        return
                    }
                    self.vm.topFreeAppList!.append((self.vm.topFreeApppResponseModel?.appList![index])!)
                }
                self.mainTableView.reloadData()
                self.loadingView.isHidden = true
            }
        } else if scrollView == self.recommandAppCollectionView {
            self.searchBar.resignFirstResponder()
            self.searchBar.text = nil
            self.vm.searchResultList = nil
        }
    }
    
    //MARK: - UICollectionViewDataSource, UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.vm.topGrossingList?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopGrossingCollectionViewCell", for: indexPath) as! TopGrossingCollectionViewCell
        guard indexPath.row < self.vm.topGrossingList?.count ?? 0 else {
            return cell
        }
        cell.appIconImageView.layer.cornerRadius = cell.appIconImageView.layer.bounds.height / 4
        cell.appIconImageView.image = UIImage(named: "placeholder")
        if let imageList = self.vm.topGrossingList?[indexPath.row].appIconList, imageList[imageList.count - 1].iconUrl?.count ?? 0 > 0 {
            cell.appIconImageView.kf.setImage(with: URL(string: imageList[imageList.count - 1].iconUrl!), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.3))])
        }
        cell.appNameLabel.text = self.vm.topGrossingList?[indexPath.row].appName ?? ""
        cell.genresLabel.text = self.vm.topGrossingList?[indexPath.row].genresString ?? ""
        
        return cell
    }

    //MARK: - UISearchBarDelegate



    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.vm.searchResultList = [AppInfoItem]()

        guard searchText.count > 0 else {
            self.suggestionTableView.isHidden = true
            self.suggestionTableView.reloadData()
            return
        }
        
        if self.vm.topFreeApppResponseModel?.appList?.count ?? 0 > 0 {
            for appInfo in self.vm.topFreeApppResponseModel!.appList! {
                if let _: Range<String.Index> = appInfo.appName?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.genresString?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.author?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.summary?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                }
            }
            self.suggestionTableView.isHidden = false
            self.suggestionTableView.reloadData()
        }
        if self.vm.topGrossingList?.count ?? 0 > 0 {
            for appInfo in self.vm.topGrossingList! {
                
                if let _: Range<String.Index> = appInfo.appName?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.genresString?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.author?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                } else if let _: Range<String.Index> = appInfo.summary?.lowercased().range(of: searchText.lowercased()) {
                    self.vm.searchResultList?.append(appInfo)
                }
            }
            self.suggestionTableView.isHidden = false
            self.suggestionTableView.reloadData()
        }
    }
    

    
    
}
