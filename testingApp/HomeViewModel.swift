//
//  HomeViewModel.swift
//  testingApp
//
//  Created by Tsz Shan Mak on 12/8/2020.
//
import RxSwift
import UIKit
import RxAlamofire

class HomeViewModel: NSObject {
    let showLoading = PublishSubject<Bool>()
    let topGrossingAppLoading = PublishSubject<Bool>()
    let topfreeAppLoading = PublishSubject<Bool>()
    
    
    var topFreeAppList: Array<AppInfoItem>?
    var topGrossingList: Array<AppInfoItem>?
    var disposedBag = DisposeBag()
    var topGrossingResponseModel: TopGrossingResponseModel?
    var topFreeApppResponseModel: TopFreeAppResponseModel?
    var searchResultList: Array<AppInfoItem>?
    
    public func requestData() {
        self.showLoading.onNext(false)
        self.topGrossingAppLoading.onNext(true)
        self.topGrossingList = [AppInfoItem]()
        
        RxAlamofire.requestString(.get, "https://itunes.apple.com/hk/rss/topgrossingapplications/limit=10/json").debug().subscribe(onNext: { [weak self](response, string) in
            self?.topGrossingResponseModel = TopGrossingResponseModel(JSONString: string)
            self?.topGrossingList = self?.topGrossingResponseModel?.appList
            self?.topGrossingAppLoading.onNext(false)
            }, onError: { [weak self](error) in
                print(error)
                self?.topGrossingAppLoading.onNext(false)
        }).disposed(by: disposedBag)
        
        
        self.topfreeAppLoading.onNext(true)
        self.topFreeAppList = [AppInfoItem]()
        RxAlamofire.requestString(.get, "https://itunes.apple.com/hk/rss/topfreeapplications/limit=100/json").debug().subscribe (onNext: { [weak self](response,string) in
            self?.topFreeApppResponseModel = TopFreeAppResponseModel(JSONString: string)
            for index in 0...10 {
                if let appInfo:AppInfoItem = self?.topFreeApppResponseModel?.appList?[index] {
                    self?.topFreeAppList!.append((appInfo))
                }
            }
            self?.topfreeAppLoading.onNext(false)
            }, onError: { [weak self] (error) in
                self?.topfreeAppLoading.onNext(false)
        }).disposed(by: disposedBag)
        
    }
    
    
}
class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var appIconImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }
    
    
}

class TopFreeAppTableviewCell: UITableViewCell {
    
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var appIconImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var ratingStv: UIStackView!
    @IBOutlet weak var ratingCountLabel: UILabel!
    var disposable: Disposable?
    let appDetailData = PublishSubject<Any>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        // CELLS STILL FREEZE EVEN WHEN THE FOLLOWING LINE IS COMMENTED OUT?!?!
        super.prepareForReuse()
        
        if self.ratingStv.arrangedSubviews.count > 0 {
            self.ratingStv.arrangedSubviews.forEach { (view) in
                self.ratingStv.removeArrangedSubview(view)
            }
        }
    }
    
    public func requestAppInfo(appId: String,index: Int) {
        self.disposable = RxAlamofire.requestString(.get, "https://itunes.apple.com/hk/lookup?id=\(String(describing: appId))").debug().subscribe (onNext: { [weak self](response,string) in
            
            if let appDatail:AppDetailResponsetModel = AppDetailResponsetModel(JSONString: string) {
                
                self?.appDetailData.onNext(appDatail)
            }
            
            }, onError: { [weak self] (error) in
                self?.appDetailData.onNext(false)
        })
        
        
    }
    
}

class TopGrossingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var appIconImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
}
